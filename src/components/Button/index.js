import React from 'react'
import PropTypes from 'prop-types'
import { sanitizeUrl } from '@braintree/sanitize-url'

import { truncate } from 'helpers'

import './style.scss'

const Button = ({ button, sendMessage }) => {
  const { value, title } = button
  const formattedTitle = title;//truncate(title, 20)

  if (button.type === 'web_url' && sanitizeUrl(value) === 'about:blank') {
    return null
  }
  let content = null
  switch (button.type) {
    case 'web_url':
      content = (
        <a
          className='RecastAppButton-Link CaiAppButton-Link'
          href={value}
          // target='_blank'
          rel='noopener noreferrer'
          data-evt-category-ga={"sap_chat_bot"}
          data-evt-action-ga={"chat_bot_button"}
          data-evt-label-ga={formattedTitle}
          data-evt-action-id = {formattedTitle}
          data-evt-action-type = "sap_chat_bot"
        >
          {formattedTitle}
        </a>
      )
      break

    default:
      content = (
        <div
          className='RecastAppButton CaiAppButton'
          data-evt-category-ga={"sap_chat_bot"}
          data-evt-action-ga={"chat_bot_button"}
          data-evt-label-ga={formattedTitle}
          data-evt-action-id = {formattedTitle}
          data-evt-action-type = "sap_chat_bot"
          onClick={() => sendMessage({ type: 'button', content: button }, title)}
        >
          {formattedTitle}
        </div>
      )
      break
  }

  return content
}

Button.propTypes = {
  button: PropTypes.object,
  sendMessage: PropTypes.func,
  onClick: PropTypes.func,
}

export default Button
