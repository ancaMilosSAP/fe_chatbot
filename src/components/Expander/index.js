import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import './style.scss'
import { withGoogleMap, GoogleMap, withScriptjs, Marker } from "react-google-maps";
import { getCredentialsFromCookie } from 'helpers'
import Geocode from "react-geocode";
Geocode.setApiKey("AIzaSyA5Csuxa6g3djdhEtnIkiL7w486F-juoOs");
import {

  getMessages,
  
} from 'actions/messages'
 
import config from '../../config'
import axios from 'axios'



class Expander extends Component {
  constructor(props) {
    super(props);
    this.state = { channelId: props.channelId, classAnimation: 'CaiAppExpander--slider', openDialog: false, setOpenDialog: false ,lat:0,lng:0,zoom:11};
   
  }
  getCookies(){
    let cookies = Object.fromEntries(document.cookie.split('; ').map(c => {
      const [ key, ...v ] = c.split('=');
      return [ key, v.join('=') ];
  }));
  return cookies;
  }
 getMessages = (channelId, token,conversationId) => {
    const client = axios.create({
      baseURL: config.apiUrl,
      headers: {
        Authorization: token,
        Accept: 'application/json',
      },
    })
  
    client.get(`/webhook/${channelId}/conversations/${conversationId}/messages`).then((res)=>{this.handleAnimationDisplay(res)})
  }
  handleAnimationDisplay(messages){
    if(messages.data.results.length>0)
    {
      let textSlider = document.querySelector("#messageText");
      textSlider.classList.remove("animationText");
      let container = document.querySelector("#sliderAnimation");
      container.classList.remove("animationContainer");
      let logoElm = document.getElementById("imgLogo");
      logoElm.classList.remove("animation");
      logoElm.classList.add("logoWithoutAnimation");
    }
  }

  componentDidMount() {
    let detailsCookies= getCredentialsFromCookie(this.state.channelId);
    if(detailsCookies!== null){
   // this.getMessages(this.state.channelId,   this.props.token,detailsCookies.conversationId);
    }

    if (this.props.wasDraw === false) {
      let logoElm = document.getElementById("imgLogo");
     // logoElm.addEventListener("mouseenter", this.triggerAnimationOnHover.bind(this));
      let expanderElm = document.getElementById("chatExpander");
      expanderElm.addEventListener("animationend", this.setNotificationVisible);
    }
    else {
      let notification = document.getElementById("notification")
      notification.style.visibility = "hidden";
    }
  }
  removeLogoAnimation() {
    let logoElm = document.getElementById("imgLogo");
    logoElm.classList.remove("animationHover");
    logoElm.classList.add("logoWithoutAnimation");
  }

  triggerAnimationOnHover() {
    if (this.props.wasDraw === false) {

      let expanderElm = document.getElementById("sliderAnimation");
      expanderElm.addEventListener("animationend", this.removeLogoAnimation);

      let logoElm = document.getElementById("imgLogo");
      logoElm.classList.remove("animation");
      logoElm.classList.remove("logoWithoutAnimation");
      logoElm.classList.remove("animationHover");
      window.setTimeout(function () {
        logoElm.classList.add("animationHover")
      }, 8);
      let textSlider = document.querySelector("#messageText");
      textSlider.classList.remove("animationText");
      textSlider.classList.remove("sliderTextFaster")
      window.setTimeout(function () {
        textSlider.classList.add("sliderTextFaster")
      }, 10);

      let container = document.querySelector("#sliderAnimation");
      container.classList.remove("animationContainer");
      container.classList.remove("sliderContainerFaster")
      window.setTimeout(function () {
        container.classList.add("sliderContainerFaster")
      }, 8);
    }
  }

  setNotificationVisible = () => {
    let notification = document.getElementById("notification");
    notification.style.visibility = "visible";
    notification.classList.remove("CaiAppExpander--notification");
    notification.classList.add("CaiAppExpander--notification");
    let chatExpander = document.getElementById("chatExpander");
    chatExpander.removeEventListener("animationend", this.setNotificationVisible);
  }

  handleClickOpen = () => {
    this.setState({ openDialog: true })
  }

  handleClose = () => {
    this.setState({ openDialog: false })
  }
  
  onPlaceSelected=(place)=>{ console.log("palce selected",place)}
  getDetailsOfAddress=(e)=>{
    if(e.keyCode==13){
    let address = document.getElementById("userAddress").value;
    Geocode.fromAddress(address).then(
      response => {
        const { lat, lng } = response.results[0].geometry.location;
        this.setState({
          lat:lat,
          lng:lng
        });
        console.log(response.results[0]);
      },
      error => {
        console.error(error);
      }
    );
    }
  }
  render() {
    let { onClick, preferences, style, show } = this.props;
    let { lat,lng } = this.state;
    let classAnimation = this.state.classAnimation;
 
    if (this.props.show === false) {
      let container = document.querySelector("#sliderAnimation");
      container.classList.remove("animationContainer");
      let logoElm = document.getElementById("imgLogo");
      logoElm.removeEventListener("mouseenter", this.triggerAnimationOnHover);
    }

    return (<div
      id="chatExpander"
      onClick={onClick}
      data-evt-category-ga = {"sap_chat_bot"} 
      data-evt-action-ga = {"open_chat_bot"} 
      data-evt-action-id = {"open_chat_bot"} 
      data-evt-action-type = {"sap_chat_bot"}
      className={cx('RecastAppExpander CaiAppExpander', { open: show, close: !show })}
    >
      <span id="imgLogo"
        className='RecastAppExpander--logo CaiAppExpander--logo animation' />
      <span id="notification"
        className='CaiAppExpander--notification'>
      </span>
      <div id="sliderAnimation" className={cx(classAnimation, "sliderContainer animationContainer")}>
        <p id="messageText" className={cx("sliderText animationText")}>{preferences.welcomeMessage}</p>
        {/* <p>Hello, I'am Alfred, your personal assistant. How can I help you?</p> */}
      </div>

    </div>)
  }
}
Expander.propTypes = {
  preferences: PropTypes.object,
  onClick: PropTypes.func.isRequired,
  style: PropTypes.object,
  show: PropTypes.bool,
}
export default Expander