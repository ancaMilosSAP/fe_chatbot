import React from 'react'
import PropTypes from 'prop-types'

import './style.scss'

const SendButton = ({ sendMessage, preferences, value }) => (
  <div className='RecastSendButtonContainer CaiSendButtonContainer'>
    <div className='RecastSendButton CaiSendButton'
      data-evt-category-ga="sap_chat_bot"
      data-evt-action-ga="chat_bot_send_Button"
      data-evt-action-type = "sap_chat_bot"
      onClick={sendMessage} disabled={!value}>
      {/* <svg width="80px" height="80px" viewBox="0 0 80 80">
    <g id="Icons-/-Controls-/-Chevron-Right" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
           </g>
  </svg> */}
      <svg
        style={{
          width: 23,
          fill: value ? preferences.accentColor : preferences.botMessageColor,
        }}
        viewBox='0 0 80 80'
      >
        <polygon id="Fill-1" fill="#18110C"
          transform="translate(59.841000, 39.746000) rotate(-90.000000) translate(-59.841000, -39.746000) "
          points="59.841 48.385 31.456 20 26 25.454 54.386 53.841 59.741 59.396 59.841 59.492 59.941 59.396 65.295 53.841 93.682 25.454 88.227 20"></polygon>
      </svg>
    </div>
  </div>
)

SendButton.propTypes = {
  preferences: PropTypes.object,
  sendMessage: PropTypes.func,
  value: PropTypes.string,
}

export default SendButton