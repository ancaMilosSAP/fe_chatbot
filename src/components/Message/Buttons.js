import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Button from 'components/Button'
import { truncate } from 'helpers'

import './style.scss'
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { withGoogleMap, GoogleMap, withScriptjs, Marker } from "react-google-maps";
import Geocode from "react-geocode";
Geocode.setApiKey("AIzaSyA5Csuxa6g3djdhEtnIkiL7w486F-juoOs");

import axios from 'axios'

// const Buttons = ({ content, sendMessage, style }) => {
//   const { title, buttons } = content
//   return (
//     <div className='Buttons'>

//       <p className='Buttons--title' >
//         {truncate(title, 640)}
//       </p>

//       <div className='Buttons--container'>
//         {buttons.slice(0, 3).map((b, i) => (
//           <Button key={i} button={b} sendMessage={sendMessage} />
//         ))}
//       </div>
//     </div>
//   )
// }
class Buttons extends Component {
  constructor(props) {
    super(props);
    this.state = { openDialog: false, setOpenDialog: false, lat: 0, lng: 0, zoom: 11, addressFromUser: "", toBeFullScreen: false, isShareLocation: false, place_id: null,skill:null }
  }
  componentDidMount() {

  }
  componentWillReceiveProps() {
    let { content } = this.props;
    let { isShareLocation } = this.state;
    const { buttons } = content;

    if (isShareLocation === false) {

      let aShareLocation = buttons.find(item => item.value.includes("SHARE_LOCATION"))
    
      if (aShareLocation != undefined) {
       let skill=  aShareLocation.value.split("&")[1];
  
        this.setState({
          isShareLocation: true,
          skill:skill
        })
        this.checkForLocation();
      }
    }
  }

  detectmob = () => {
    if (navigator.userAgent.match(/Android/i)
      || navigator.userAgent.match(/webOS/i)
      || navigator.userAgent.match(/iPhone/i)
      || navigator.userAgent.match(/iPod/i)
      || navigator.userAgent.match(/BlackBerry/i)
      || navigator.userAgent.match(/Windows Phone/i)
    ) {
      return true;
    }
    else {
      return false;
    }
  }

  getDetailsOfAddress = (e) => {
    if (e.keyCode == 13) {
      let address = document.getElementById("userAddress").value;
      Geocode.fromAddress(address).then(
        response => {
          const { lat, lng } = response.results[0].geometry.location;
          const { place_id, formatted_address } = response.results[0];
          this.setState({
            lat: lat,
            lng: lng,
            place_id: place_id,
            formatted_address: formatted_address,
            addressFromUser: formatted_address
          });
        },
        error => {
          console.error(error);
        }
      );
    }
  }

  handleClickOpen = () => {
    document.getElementById("RecastAppChatHolder").setAttribute("style", "z-index:11");
    this.checkForLocation();
    this.setState({ openDialog: true })
    if (this.detectmob() === true) {
      this.setState({
        toBeFullScreen: true
      });
    }
  }

  checkForLocation = () => {
    navigator.geolocation.getCurrentPosition(position => {
      let positionDetails = position.coords;
      const { latitude, longitude } = positionDetails;
      Geocode.fromLatLng(latitude, longitude).then(
        response => {
          const address = response.results[0].formatted_address,
            addressArray = response.results[0].address_components,
            city = this.getCity(addressArray),
            place_id = response.results[0].place_id;
          this.setState({
            lat: latitude,
            lng: longitude,
            addressFromUser: address,
            place_id: place_id
          });
        },
        error => {
          console.error(error);
        }
      );


    })
  }
  getCity = (addressArray) => {
    let city = '';
    for (let i = 0; i < addressArray.length; i++) {
      if (addressArray[i].types[0] && 'administrative_area_level_2' === addressArray[i].types[0]) {
        city = addressArray[i].long_name;
        return city;
      }
    }
  }
  handleClose = () => {
    this.setState({ openDialog: false })
  }

  updateMemory = () => {
    const { conversation } = this.props;
    let { formatted_address, lng, lat, place_id ,skill,addressFromUser} = this.state;
    window.webchatMethods = {
      getMemory: (conversation) => {
        const memory = {
          "location": {
            "formatted": formatted_address !== undefined ? formatted_address : addressFromUser,
            "place":  place_id,
          },
          "skill" : skill,
        }
        return { memory, merge: true }
      }
    }
  }

  sendMessageExtended = () => {
    this.updateMemory(); 
    document.getElementById("RecastAppChatHolder").setAttribute("style", "z-index:2147483647");
    document.getElementById("RecastAppChatHolder").setAttribute("style", "background-color: rgb(255, 255, 255)");
    let { sendMessage } = this.props;
    let { addressFromUser, lng, lat, formatted_address } = this.state;

    let attachment = {
      "content": {
        "value":"USER_LOCATION_"+lng+"_"+lat,
        "userAddress": formatted_address !== undefined ? formatted_address : addressFromUser,
        "lng": lng,
        "lat": lat
      },
      "type": "map"
    }
    sendMessage(attachment, "yes");
  }

  reCenterMap=()=>{
    let { lat, lng} = this.state;
    // this.setState({
    //   lat: lat,
    //   lng: lng,
    // });

  }
  render() {
    let { content, sendMessage, style } = this.props;
    const { title, buttons } = content;
    let { openDialog, lat, lng, addressFromUser, toBeFullScreen,skill } = this.state;
    const MapWithAMarker = withScriptjs(withGoogleMap(() =>
      <GoogleMap
        defaultZoom={13}
        defaultCenter={{ lat: lat, lng: lng }}
      >
        <Marker
          draggable={true}
          position={{ lat: lat, lng: lng }}
        />
      </GoogleMap>
    ));

    let dialogMapContent =
      <div className="mapContentHolder">
        <div className="headerMapHolder">
          <div className="headerMapItems">
          <div id="mapDialogTitle">
            <span>LOCALIZATION</span>
          </div>
          <div className="imageCloseHolder">
            <img onClick={this.handleClose} className="closeMapButton" src="https://eu.louisvuitton.com/images/is/image/lv/back@3x.png"></img>
          </div>
          </div>
        </div>
        <div className="inputMap">
          <div><img className="seacrhIconMap" src="https://eu.louisvuitton.com/images/is/image/lv/search@3x.png"></img></div>
          <div><input id="userAddress" placeholder="Your location" onKeyDown={this.getDetailsOfAddress} /></div>
          <div onClick={this.reCenterMap}><img className="trackIconMap" src="https://eu.louisvuitton.com/images/is/image/lv/trackorder@3x.png"></img> </div>
        </div>
        <DialogContent className="contentDilogMap">
          <MapWithAMarker
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyA5Csuxa6g3djdhEtnIkiL7w486F-juoOs&v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div className="loadingElementDiv" />}
            containerElement={<div className="containerElementDiv" />}
            mapElement={<div className="mapElementDiv" />}
          />
        </DialogContent>
        <div className="dialogActionCustom">
          <div className="divHolderActionItems" onClick={this.handleClose}>
            <div className="addressHolder"><span>{addressFromUser}</span></div>
            <div className="buttonDirectionHolder" onClick={this.sendMessageExtended}>
              <div className="buttonDirectionItems">
                <img className="iconGo" src="https://eu.louisvuitton.com/images/is/image/lv/direction3x.png"></img>
                <span className="textDirection"> Confirm location</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    let DialogWithMap;
    if (toBeFullScreen) {
      DialogWithMap =
        <Dialog
          id="mapElement"
          open={openDialog}
          fullScreen
          onClose={this.handleClose}>
          {dialogMapContent}
        </Dialog>
    } else {
      DialogWithMap =
        <Dialog
          id="mapElement"
          open={openDialog}
          onClose={this.handleClose}>
          {dialogMapContent}
        </Dialog>
    }
    return (
      <div>
        <div className='Buttons'>
          <p className='Buttons--title' >
            {truncate(title, 640)}
          </p>
          <div className='Buttons--container'>
            {buttons.map((b, i) => {
              if (b.value.includes("SHARE_LOCATION")) {
                let keyB =Math.random();
                return (
                  <div className="RecastAppButton CaiAppButton">
                    <button  key={keyB} onClick={this.handleClickOpen}>{b.title}</button>
                  </div>
                )
              }
              else {
                return <Button key={i} button={b} sendMessage={sendMessage} />
              }
            })}
            {/* <button onClick={this.handleClickOpen}>Share location</button> */}
          </div>
        </div>
        {DialogWithMap}
      </div>
    )
  }
}


Buttons.propTypes = {
  style: PropTypes.object,
  content: PropTypes.object,
  sendMessage: PropTypes.func,
  onClick: PropTypes.func,
}

export default Buttons
