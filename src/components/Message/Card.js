import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { sanitizeUrl } from '@braintree/sanitize-url'

import { truncate } from 'helpers'

import Button from 'components/Button'

class Card extends Component {
  constructor(props) {
    super(props);
  }
  detectmob = () => {
    if (navigator.userAgent.match(/Android/i)
      || navigator.userAgent.match(/webOS/i)
      || navigator.userAgent.match(/iPhone/i)
      || navigator.userAgent.match(/iPad/i)
      || navigator.userAgent.match(/iPod/i)
      || navigator.userAgent.match(/BlackBerry/i)
      || navigator.userAgent.match(/Windows Phone/i)
    ) {
      return true;
    }
    else {
      return false;
    }
  }
  render() {
    let { content, sendMessage, onImageLoaded } = this.props;
    const {  imageUrl, buttons } = content
    let {subtitle,title} = content;
    let classAdded = " text";
    let typeCarousel = "--complete"
    if (imageUrl && sanitizeUrl(imageUrl) === 'about:blank') {
      return null
    }
    if (imageUrl !== "") {
      classAdded = "img"
    }
    if (imageUrl === "" && title === "") {
      // carousel with qa  and see answer
      typeCarousel = "--simple"
    }
  
    if (subtitle !== "" && subtitle !== undefined && subtitle !== null && (subtitle.indexOf("&#039;") >= 0)) {
      subtitle = subtitle.replace("&#039;", "'");
    }
    if (title !== "" && title !== undefined && title !== null && (title.indexOf("&#039;") >= 0)) {
      title = title.replace("&#039;", "'");
      title = truncate(title, 80);
    }
    let titleEdited = title;
    let editedSubtitle = subtitle
    let subTitleHTML = subtitle
  
    if (editedSubtitle !== undefined) {
      let startBr = editedSubtitle.indexOf("<br>");
      if (startBr != -1) {
        let startNewSubtitle = editedSubtitle.substr(0, startBr);
        let endNewSubtitle = editedSubtitle.substr(startBr + 4);
        let numberTag = endNewSubtitle;
        if (this.detectmob() === true) {
          let hrefTel = "tel:" + editedSubtitle.substr(startBr + 4);
          numberTag = <a href={hrefTel}><b>{endNewSubtitle}</b></a>
        }
        subTitleHTML = <p>{startNewSubtitle} <br></br>{numberTag}</p>
      }
    }
    return (
      <div className={'RecastAppCard CaiAppCard'}>
        {imageUrl && (
          <img src={imageUrl} onLoad={onImageLoaded} className='RecastAppCard--img CaiAppCard--img' />
        )}

        <div className={'RecastAppCard--text CaiAppCard--text' + classAdded}>
          <div className={'RecastAppCard--text CaiAppCard--TitleSubtitleHolder' + typeCarousel}>
            <p className='RecastAppCard--text-title CaiAppCard--text-title'>{titleEdited}</p>
            {subtitle && <span className={'Card--text-subtitle' + typeCarousel}>{subTitleHTML}</span>}
          </div>
        </div>

        {buttons.length ? (
          <div className='RecastAppCard--button-container CaiAppCard--button-container'>
            {buttons.slice(0, 3).map((b, i) => (
              <Button key={i} button={b} sendMessage={sendMessage} />
            ))}
          </div>
        ) : null}
      </div>
    )
  }
}
Card.propTypes = {
  content: PropTypes.object,
  sendMessage: PropTypes.func,
  onImageLoaded: PropTypes.func,
}

export default Card

// const Card = ({ content, sendMessage, onImageLoaded }) => {
//   const { title, subtitle, imageUrl, buttons } = content
//   let classAdded=" text";
//   let typeCarousel ="--complete"
//   if (imageUrl && sanitizeUrl(imageUrl) === 'about:blank') {
//     return null
//   }
//   if(imageUrl!==""){
//     classAdded = "img"
//   }
//   if(imageUrl==="" && title===""){
//     // carousel with qa  and see answer
//     typeCarousel="--simple"
//   }



//   let editedSubtitle = subtitle;
//   let startBr = editedSubtitle.indexOf("<br>");
//   let startNewSubtitle = editedSubtitle.substr(0,startBr);
//   let endNewSubtitle = editedSubtitle.substr(startBr+4);
//   let hrefTel = "tel:"+editedSubtitle.substr(startBr+4);
//   let subTitleHTML = <p>{startNewSubtitle} <br></br><a href={hrefTel}>{endNewSubtitle}</a></p>


//   return (
//     <div className={'RecastAppCard CaiAppCard'}>
//       {imageUrl && (
//         <img src={imageUrl} onLoad={onImageLoaded} className='RecastAppCard--img CaiAppCard--img' />
//       )}

//       <div className={'RecastAppCard--text CaiAppCard--text'+classAdded}>
//         <div className={'RecastAppCard--text CaiAppCard--TitleSubtitleHolder'+typeCarousel}>
//         <p className='RecastAppCard--text-title CaiAppCard--text-title'>{truncate(title, 80)}</p>
//         {subtitle && <span className={'Card--text-subtitle'+typeCarousel}>{subTitleHTML}</span>}
//         </div>
//       </div>

//       {buttons.length ? (
//         <div className='RecastAppCard--button-container CaiAppCard--button-container'>
//           {buttons.slice(0, 3).map((b, i) => (
//             <Button key={i} button={b} sendMessage={sendMessage} />
//           ))}
//         </div>
//       ) : null}
//     </div>
//   )
// }

// Card.propTypes = {
//   content: PropTypes.object,
//   sendMessage: PropTypes.func,
//   onImageLoaded: PropTypes.func,
// }

// export default Card
