
import React, { Component } from "react";
import { withGoogleMap, GoogleMap, withScriptjs, Marker } from "react-google-maps";
import PropTypes from 'prop-types'
import { truncate } from 'helpers'


class Map extends Component{
  shouldComponentUpdate(nextProps){
    return nextProps.content !== this.props.content
  }

  render (){
    let { content } = this.props;
    let { lat, lng, userAddress } = content;
  
    const MapWithAMarker = withScriptjs(withGoogleMap(() =>
      <GoogleMap
        defaultZoom={15}
        defaultCenter={{ lat: lat, lng: lng }}
        options={{
          gestureHandling:'none'
        }}
      >
        <Marker
          position={{ lat: lat, lng: lng }}
        />
      </GoogleMap>
    ));
    return (
      <div className={'RecastAppCard CaiAppCard Map'}>
        <MapWithAMarker
          className="MapWithAMarker"
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyA5Csuxa6g3djdhEtnIkiL7w486F-juoOs&v=3.exp&libraries=geometry,drawing,places"
          loadingElement={<div className="loadingElementMap" />}
          containerElement={<div className="containerElementMap" />}
          mapElement={<div className="mapElementMap" />} 
          />
        <div className={'RecastAppCard--text CaiAppCard--text'}>
          <div className={'RecastAppCard--text CaiAppCard--TitleSubtitleHolder-Map'}>
            <p className='RecastAppCard--text-title CaiAppCard--text-title-Map'>{truncate(userAddress, 80)}</p>
            {userAddress && <span className={'Card--text-subtitle'}>{userAddress}</span>}
          </div>
        </div>
      </div>
    )
  }
}
export default Map