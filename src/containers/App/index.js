import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Chat from 'containers/Chat'
import Expander from 'components/Expander'
import { setFirstMessage, removeAllMessages } from 'actions/messages'
import { setCredentials, createConversation } from 'actions/conversation'
import { storeCredentialsInCookie, getCredentialsFromCookie } from 'helpers'

import './style.scss'

const NO_LOCALSTORAGE_MESSAGE
  = 'Sorry, your browser does not support web storage. Are you in localhost ?'

@connect(
  state => ({
    isReady: state.conversation.conversationId,
  
    }),
  {
  setCredentials,
  setFirstMessage,
  createConversation,
  removeAllMessages,
  },
)
class App extends Component {
  state = {
    expanded: this.props.expanded || false,
    wasDraw:false,
    token:null
  }

  componentDidMount () {
    const { channelId, token, preferences, noCredentials, onRef } = this.props
    const credentials = getCredentialsFromCookie(channelId)
    const payload = { channelId, token }

    if (onRef) {
      onRef(this)
    }

    if (noCredentials) {
      return false
    }

    if (credentials) {
      Object.assign(payload, credentials)
    } else {
      this.props.createConversation(channelId, token).then(({ id, chatId }) => {
        storeCredentialsInCookie(chatId, id, preferences.conversationTimeToLive, channelId)
      })
    }
    if (preferences.welcomeMessage || preferences.welcomeMessage=== undefined) {
      let messagesStart =[];
      messagesStart= [
          {
            "type": "buttons",
            "content": {
              "title": "I'm here to assist and guide you as you shop. What can I help you with? You can click on the buttons below to select a topic or directly type your question.",
              "buttons": [
                {
                  "value": "CHECK_STORE_AVAILABILITY",
                  "title": "Store Availability",
                  "type": "postback"
                },
                {
                  "value": "REQUEST_ASSISTANCE",
                  "title": "LV Assistance",
                  "type": "postback"
                },
                {
                  "value": "PRODUCT_DISCOVERY_FROM_MENU",
                  "title": "Product Discovery",
                  "type": "postback"
                },
                {
                  "value": "Contact an Advisor",
                  "title": "Contact an Advisor",
                  "type": "postback"
                }
              ]
            }
          },
          {
            "type": "text",
            "content": "Hello, I'm glad to meet you. I am your Louis Vuitton Virtual Assistant.",
            "markdown": null,
            "delay": null
          }
        ],
        messagesStart.forEach(item=>{
          this.props.setFirstMessage(item)
        })
      
    //  this.props.setFirstMessage(preferences.welcomeMessage)
    }

    this.props.setCredentials(payload)
   
  }

  componentWillReceiveProps (nextProps) {
    const { isReady, preferences, expanded } = nextProps

    if (isReady !== this.props.isReady) {
      let expanded = null

      switch (preferences.openingType) {
      case 'always':
        expanded = true
        break
      case 'never':
        expanded = false
        break
      case 'memory':
        if (typeof window.localStorage !== 'undefined') {
          expanded = localStorage.getItem('isChatOpen') === 'true'
        } else {
          console.log(NO_LOCALSTORAGE_MESSAGE)
        }
        break
      default:
        break
      }
      this.setState({ expanded })
    }

    if (expanded !== undefined && expanded !== this.state.expanded) {
      this.setState({ expanded })
    }
  }

  componentDidUpdate (prevState) {
    const { onToggle } = this.props

    if (prevState.expanded !== this.state.expanded) {
      if (typeof window.localStorage !== 'undefined') {
        localStorage.setItem('isChatOpen', this.state.expanded)
        if (onToggle) {
          onToggle(this.state.expanded)
        }
      } else {
        console.log(NO_LOCALSTORAGE_MESSAGE)
      }
    }
  }

  componentDidCatch (error, info) {
    console.log(error, info)
  }

  toggleChat = () => {
  document.getElementById("notification").style.visibility="hidden";
  let container = document.querySelector("#sliderAnimation");
  container.classList.remove("sliderContainerFaster")
  container.classList.remove("sliderContainer")
  let logoElm = document.getElementById("imgLogo");
  logoElm.classList.remove("animation");
  logoElm.classList.remove("animationHover");
  logoElm.classList.add("logoWithoutAnimation");
  this.setState({wasDraw:true});
    const { clearMessagesOnclose } = this.props
    this.setState({ expanded: !this.state.expanded }, () => {
      if (!this.state.expanded && clearMessagesOnclose) {
        this.clearMessages()
      }
    })
  }

  clearMessages = () => {
    this.props.removeAllMessages()
  }

  render () {
    const {
      preferences,
      containerMessagesStyle,
      containerStyle,
      expanderStyle,
      logoStyle,
      showInfo,
      sendMessagePromise,
      onClickShowInfo,
      primaryHeader,
      secondaryView,
      secondaryHeader,
      secondaryContent,
      getLastMessage,
      enableHistoryInput,
      defaultMessageDelay,
      channelId,
      token
    } = this.props
    const { expanded } = this.state

    return (
      <div className='RecastApp CaiApp'>
        <link
          rel='stylesheet'
          type='text/css'
          href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css'
        />
        <link
          rel='stylesheet'
          type='text/css'
          href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css'
        />

        <Expander
          show={!expanded}
          onClick={this.toggleChat}
          preferences={preferences}
          style={expanderStyle}
          wasDraw ={this.state.wasDraw} 
          channelId = {channelId}
          token={token}
        />

        <Chat
          show={expanded}
          closeWebchat={this.toggleChat}
          preferences={preferences}
          containerMessagesStyle={containerMessagesStyle}
          containerStyle={containerStyle}
          logoStyle={logoStyle}
          showInfo={showInfo}
          onClickShowInfo={onClickShowInfo}
          sendMessagePromise={sendMessagePromise}
          primaryHeader={primaryHeader}
          secondaryView={secondaryView}
          secondaryHeader={secondaryHeader}
          secondaryContent={secondaryContent}
          getLastMessage={getLastMessage}
          enableHistoryInput={enableHistoryInput}
          defaultMessageDelay={defaultMessageDelay}
        />
      </div>
    )
  }
}

App.propTypes = {
  token: PropTypes.string,
  channelId: PropTypes.string,
  preferences: PropTypes.object.isRequired,
  containerMessagesStyle: PropTypes.object,
  expanderStyle: PropTypes.object,
  containerStyle: PropTypes.object,
  showInfo: PropTypes.bool,
  sendMessagePromise: PropTypes.func,
  noCredentials: PropTypes.bool,
  primaryHeader: PropTypes.func,
  secondaryView: PropTypes.bool,
  secondaryHeader: PropTypes.any,
  secondaryContent: PropTypes.any,
  getLastMessage: PropTypes.func,
  expanded: PropTypes.bool,
  onToggle: PropTypes.func,
  removeAllMessages: PropTypes.func,
  onRef: PropTypes.func,
  clearMessagesOnclose: PropTypes.bool,
  enableHistoryInput: PropTypes.bool,
  defaultMessageDelay: PropTypes.number,
}

export default App
